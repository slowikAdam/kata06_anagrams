package kata06_anagrams;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class AnagramsTest {

	@Test
	public void testPrintSolution() {
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		PrintStream tmp = System.out;
		System.setOut(new PrintStream(outContent));

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		List<String> list = new ArrayList<String>();
		list.add("abc");
		list.add("cba");
		list.add("bca");
		map.put("xxx", list);
		list = new ArrayList<String>();
		list.add("wer");
		list.add("rew");
		list.add("erw");
		map.put("yyy", list);
		Anagrams.printSolution(map);
		System.setOut(tmp);
		String result = outContent.toString();
		String expResult ="wer; rew; erw; \r\n" + 
		"abc; cba; bca; \r\n";
		assertTrue(result.startsWith(expResult));
	}

}
