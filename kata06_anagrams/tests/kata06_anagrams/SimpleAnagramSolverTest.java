package kata06_anagrams;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class SimpleAnagramSolverTest {

	@Test
	public void testCalculateKey() {
		SimpleAnagramSolver sas=new SimpleAnagramSolver();
		assertEquals("aabbcc", sas.calculateKey("abcabc"));
	}
	
	@Test
	public void testSolve() {
		SimpleAnagramSolver sas=new SimpleAnagramSolver();
		Set<String> set =new HashSet<String>();
		set.add("abc");
		set.add("bca");
		set.add("cba");
		set.add("def");
		set.add("fed");
		Map<String, List<String>> result=sas.solve(set);
		
		assertTrue(result.containsKey("abc"));
		assertTrue(result.get("abc").contains("abc"));
		assertTrue(result.get("abc").contains("bca"));
		assertTrue(result.get("abc").contains("cba"));
		
		assertTrue(result.containsKey("def"));
		assertTrue(result.get("def").contains("def"));
		assertTrue(result.get("def").contains("fed"));
		
	}

}
