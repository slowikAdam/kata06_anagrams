package kata06_anagrams;

import static org.junit.Assert.*;


import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class ParallelAnagramSolverTest {

	@Test
	public void testSolve() throws FileNotFoundException {
		Set<String> dict = Anagrams.readFile("dicts\\wordlist.txt");
		Map<String, List<String>> result1=new SimpleAnagramSolver().solve(dict);
		Map<String, List<String>> result2=new ParallelAnagramSolver().solve(dict);
		//comparing results from 'SimpleAnagramSolver' and 'ParallelAnagramSolver'
		//'SimpleAnagramSolver' is well tested (therefore is correct), if results from 'ParallelAnagramSolver' are same then both implementations are correct
		for (String key : result1.keySet()) {
			List<String> list1 =result1.get(key);
			assertTrue(result2.containsKey(key));
			List<String> list2 =result2.get(key);
			assertTrue(list1.size()==list2.size());
			for (String element : list1) {
				assertTrue(list2.contains(element));
			}
		}
	}

}
