package kata06_anagrams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

class SimpleAnagramSolver implements AnagramSolver{

	@Override
	public Map<String, List<String>> solve(Set<String> dict) {
		Map<String, List<String>> result=new HashMap<String, List<String>>();
		String key=null; 
		for (String element:dict) {
			key=calculateKey(element);
			if (result.containsKey(key)) {
				result.get(key).add(element);
			} else {
				List<String> tmp =new ArrayList<String>();
				tmp.add(element);
				result.put(key, tmp);
			}
		}
		return result;
	}

	String calculateKey(String element) {
		char[] array=element.toCharArray();
		Arrays.sort(array);		
		return new String(array);
	}

}
