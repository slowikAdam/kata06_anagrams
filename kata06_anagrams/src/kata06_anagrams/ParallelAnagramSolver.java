package kata06_anagrams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class ParallelAnagramSolver implements AnagramSolver {
	@Override
	public Map<String, List<String>> solve(Set<String> dict) {
		
		ConcurrentMap<String,List<String>> result=dict.parallelStream().collect(Collectors.groupingByConcurrent(xxx -> this.calculateKey(xxx)));
		return result;
	}

	String calculateKey(String element) {
		char[] array=element.toCharArray();
		Arrays.sort(array);		
		return new String(array);
	}
}
