package kata06_anagrams;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Anagrams {
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		String path = null;
		if (args.length == 0) {
			// if args are empty use defaults
			path = "dicts\\wordlist.txt";
		} else {
			// if possible load params from args
			path = args[0];
		}
		Set<String> dict;
		try {
			dict = readFile(path);
		} catch (FileNotFoundException e) {
			System.out.println("File " + path + " not found. Please ensure that the specified file exists.");
			// TODO print info about usage
			return;
		}
		if (dict == null) {
			// TODO print info about usage
			return;
		}
		long dictLoaded = System.currentTimeMillis();
		long firstAtemptStart = System.currentTimeMillis();
		AnagramSolver simpleAnagramSolver = new SimpleAnagramSolver();
		printSolution(simpleAnagramSolver.solve(dict));
		long firstAtemptEnd = System.currentTimeMillis();
		long secondAtemptStart = System.currentTimeMillis();
		AnagramSolver parallelAnagramSolver = new ParallelAnagramSolver();
		printSolution(parallelAnagramSolver.solve(dict));
		long secondAtemptEnd = System.currentTimeMillis();
		System.out.println("dict loaded in " + (dictLoaded - startTime) + "[ms]");
		System.out.println("First atepmpt. Solution found in " + (firstAtemptEnd - firstAtemptStart) + "[ms]");
		System.out.println("Second atepmpt. Solution found in " + (secondAtemptEnd - secondAtemptStart) + "[ms]");
	}

	static void printSolution(Map<String, List<String>> result) {
		for (String key : result.keySet()) {
			List<String> list =result.get(key);
			if (list.size()<2) {
				continue;
			}
			for (String element : list) {
				System.out.print(element + "; ");
			}
			System.out.println("");
		}

	}

	/**
	 * 
	 * @param path
	 *            file Path
	 * @return set with all words from given file. Duplicates are removed. All words
	 *         are uppercase.
	 * @throws FileNotFoundException
	 */
	static Set<String> readFile(String path) throws FileNotFoundException {
		BufferedReader inputStream = null;

		inputStream = new BufferedReader(new FileReader(path));

		Set<String> set = new HashSet<String>();
		try {
			String line = null;
			while ((line = inputStream.readLine()) != null)
				set.add(line.toUpperCase());
		} catch (IOException e) {
			System.out.println("error reading File");
		} finally {
			System.out.println("closing file");
			try {
				inputStream.close();
				System.out.println("file closed succesfully");
			} catch (IOException e) {
				System.out.println("closing file failed!");
			}
		}
		return set;
	}
}
