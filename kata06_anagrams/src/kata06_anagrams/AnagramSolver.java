package kata06_anagrams;

import java.util.List;
import java.util.Map;
import java.util.Set;

interface AnagramSolver {
	abstract Map<String,List<String>> solve(Set<String> dict);
}
